const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {
  getAll() {
    const items = MessageRepository.getAll();
    if (!items) {
      throw Error('List of messages is empty.');
    }
    return items;
  }

  getOne(id) {
    const item = MessageRepository.getOne({ id });
    if (!item) {
      throw Error('Message doesn\'t exist.');
    }
    return item;
  }

  create(message) {
    const item = MessageRepository.create(message);
    if (!item) {
      throw Error('Message wasn\'t successfully created.');
    }
    return item;
  }

  update(id, message) {
    const item = MessageRepository.update(id, message);
    if (!item.id) {
      throw Error(`There isn't any message with id ${id} to update.`);
    }
    return item;
  }

  delete(id) {
    const item = MessageRepository.delete(id);
    if (!item.length) {
      throw Error(`There isn't any message with id ${id} to delete.`);
    }
    return item;
  }

  search(search) {
    const item = MessageRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new MessageService();
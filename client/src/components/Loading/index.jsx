import React from 'react';
import styles from './loading.module.scss';

const Loading = () => (
  <div className={styles.spiner}>
    <div />
    <div />
    <div />
  </div>
);

export default Loading;

import React from 'react';
import PropTypes from 'prop-types';

const IconBtn = ({ className, iconClasses, onClick }) => (
  <button type="button" className={className} onClick={onClick}>
    <i className={iconClasses} />
  </button>
);

IconBtn.propTypes = {
  className: PropTypes.string,
  iconClasses: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

IconBtn.defaultProps = {
  className: '',
};

export default IconBtn;

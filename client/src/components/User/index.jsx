import React from 'react';
import PropTypes from 'prop-types';
import styles from './user.module.scss';

const User = ({ user, deleteUser, editUser }) => {
  const {
    id, name, email,
  } = user;

  return (
    <div className={styles.container}>
      <div className={styles.userInfo}>
        <div className={styles.userName}>
          {name}
        </div>
        <div className={styles.email}>
          {email}
        </div>
      </div>
      <button type="button" onClick={() => deleteUser(id)}>Edit</button>
      <button type="button" onClick={editUser}>Delete</button>
    </div>
  );
};

User.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    email: PropTypes.string,
    password: PropTypes.string,
    role: PropTypes.string,
  }).isRequired,
  deleteUser: PropTypes.func.isRequired,
  editUser: PropTypes.func.isRequired,
};

export default User;

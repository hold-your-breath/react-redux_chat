import { combineReducers } from 'redux';
import chatReducer from '../pages/Chat/reducer';
import userListReducer from '../pages/UserList/reducer';

const rootReducer = combineReducers({
  messages: chatReducer,
  users: userListReducer,
});

export default rootReducer;

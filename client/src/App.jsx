import React from 'react';
import { Route, Switch } from 'react-router-dom';
import styles from './App.module.scss';
import Logo from './components/Logo';
import Chat from './pages/Chat';
import Login from './pages/Login';
import MessageEditor from './pages/MessageEditor';
import UserEditor from './pages/UserEditor';
import UserList from './pages/UserList';

const NotFound = () => (
  <div>Not Found</div>
);

const App = () => (
  <div className={styles.appContainer}>
    <Logo />
    <Switch>
      <Route exact path="/" component={Chat} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/edit-message/:id" component={MessageEditor} />
      <Route exact path="/users" component={UserList} />
      <Route exact path="/users/:id" component={UserEditor} />
      <Route component={NotFound} />
    </Switch>
  </div>
);

export default App;

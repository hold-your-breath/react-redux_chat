import {
  get, post, put, deleteReq,
} from '../requestHelper';

const entity = 'messages';

export const getMessages = async () => await get(entity);

export const createMessage = async (body) => await post(entity, body);

export const updateMessage = async (body) => await put(entity, body.id, body);

export const deleteMessage = async (id) => await deleteReq(entity, id);

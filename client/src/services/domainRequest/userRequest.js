import {
  deleteReq, get, post, put,
} from '../requestHelper';

const entity = 'users';

export const getUsers = async () => await get(entity);

export const createUser = async (body) => await post(entity, body);

export const updateUser = async (body) => await put(entity, body.id, body);

export const deleteUser = async (id) => await deleteReq(entity, id);

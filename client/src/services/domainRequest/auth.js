import { post } from '../requestHelper';

export default async (body) => await post('auth/login', body);

import { API_URL } from '../shared/constants';

export const get = async (entityName, id = '') => await makeRequest(`${entityName}/${id}`, 'GET');

export const post = async (entityName, body) => await makeRequest(entityName, 'POST', body);

export const put = async (entityName, id, body) => await makeRequest(`${entityName}/${id}`, 'PUT', body);

export const deleteReq = async (entityName, id) => await makeRequest(`${entityName}/${id}`, 'DELETE');

// eslint-disable-next-line consistent-return
const makeRequest = async (path, method, body) => {
  try {
    const url = `${API_URL}/${path}`;
    const res = await fetch(url, {
      method,
      body: body ? JSON.stringify(body) : undefined,
      headers: { 'Content-Type': 'application/json' },
    });

    const dataObj = await res.json();

    if (res.ok) {
      return dataObj;
    }

    // eslint-disable-next-line no-alert
    alert(`${dataObj.message}`);
    return dataObj;
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
  }
};

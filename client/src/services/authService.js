import { setLocalStorageItem, getObjectFromLocalStorage } from './localStorageHelper';

const entity = 'user';

export const isSignedIn = () => {
  const user = getObjectFromLocalStorage(entity);
  return !!user;
};

export const setLoginSession = (user) => {
  setLocalStorageItem(entity, user);
};

export const unsetLoginSession = () => {
  setLocalStorageItem(entity, null);
};

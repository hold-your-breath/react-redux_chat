import { LOG_IN, LOGGED_IN_SUCCESS, LOGGED_IN_FAILED } from './actionTypes';

export const logInSuccess = (user) => ({
  type: LOGGED_IN_SUCCESS,
  payload: {
    user,
  },
});

export const logIn = () => ({
  type: LOG_IN,
});

export const logInFailed = (err) => ({
  type: LOGGED_IN_FAILED,
  payload: {
    err,
  },
});

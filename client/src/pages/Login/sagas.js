/* eslint-disable import/prefer-default-export */
import {
  all, call, put, takeEvery,
} from 'redux-saga/effects';
import { LOGGED_IN_FAILED, LOGGED_IN_SUCCESS, LOG_IN } from './actionTypes';
import login from '../../services/domainRequest/auth';

export function* logIn(action) {
  try {
    const user = { ...action.payload.data };
    yield call(login(user));
    yield put({ type: LOGGED_IN_SUCCESS });
  } catch (err) {
    yield put({ type: LOGGED_IN_FAILED, payload: { error: err } });
  }
}

function* watchLoggedInUsers() {
  yield takeEvery(LOG_IN, logIn);
}

export default function* loginSagas() {
  yield all([
    watchLoggedInUsers(),
  ]);
}

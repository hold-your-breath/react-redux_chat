import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import styles from './login.module.scss';
import { logIn } from './actions';

const Login = ({ logIn: logInUser }) => {
  const history = useHistory();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const blocCubeInput = [styles.control, styles.blockCube, styles.blockInput];

  const userLogIn = () => {
    if (username && password) {
      logInUser({ username, password });
      setUsername('');
      setPassword('');
      history.push('/');
    }
  };

  return (
    <div className={styles.container}>
      <form className={styles.form}>
        <div className={styles.control}>
          <h1>Sign In</h1>
        </div>
        <div className={blocCubeInput.join(' ')}>
          <input name="username" placeholder="Username" type="text" value={username} onChange={(e) => setUsername(e.target.value)} />
          <div className={styles.bgTop}>
            <div className={styles.bgInner} />
          </div>
          <div className={styles.bgRight}>
            <div className={styles.bgInner} />
          </div>
          <div className={styles.bg}>
            <div className={styles.bgInner} />
          </div>
        </div>
        <div className={blocCubeInput.join(' ')}>
          <input name="password" placeholder="Password" type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
          <div className={styles.bgTop}>
            <div className={styles.bgInner} />
          </div>
          <div className={styles.bgRight}>
            <div className={styles.bgInner} />
          </div>
          <div className={styles.bg}>
            <div className={styles.bgInner} />
          </div>
        </div>
        <button className={[styles.btn, styles.blockCube, styles.blockCubeHover].join(' ')} type="button" onClick={userLogIn}>
          <div className={styles.bgTop}>
            <div className={styles.bgInner} />
          </div>
          <div className={styles.bgRight}>
            <div className={styles.bgInner} />
          </div>
          <div className={styles.bg}>
            <div className={styles.bgInner} />
          </div>
          <div className={styles.text}>Log In</div>
        </button>
      </form>
    </div>
  );
};

Login.propTypes = {
  logIn: PropTypes.func.isRequired,
};

const actions = {
  logIn,
};

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps,
)(Login);

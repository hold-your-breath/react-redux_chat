import {
  all, call, put, takeEvery,
} from 'redux-saga/effects';
import { updateMessage as update } from '../../services/domainRequest/messageRequest';
import { UPDATE_MESSAGE, UPDATE_MESSAGE_FAILED, UPDATE_MESSAGE_SUCCESS } from './actionTypes';

export function* updateMessage(action) {
  const updatedMessage = { ...action.payload.data };
  try {
    yield call(update(updatedMessage));
    yield put({ type: UPDATE_MESSAGE_SUCCESS });
  } catch (err) {
    yield put({ type: UPDATE_MESSAGE_FAILED });
  }
}

function* watchUpdateMessage() {
  yield takeEvery(UPDATE_MESSAGE, updateMessage);
}

export default function* messageEditorSagas() {
  yield all([
    watchUpdateMessage(),
  ]);
}

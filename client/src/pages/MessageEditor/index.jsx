import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styles from './messageEditor.module.scss';

const MessageEditor = ({ message }) => {
  const [newMessage, setNewMessage] = useState('');

  useEffect(() => {
    if (message.text) {
      setNewMessage(message.text);
    }
  }, [message.text]);

  const save = () => {
    /* Save */
  };

  const cancel = () => {
    /* Cancel */
  };

  return (
    <div className={styles.container}>
      <div className={styles.header}>Edit message</div>
      <textarea value={newMessage} onChange={(e) => setNewMessage(e.target.value)} />
      <div className={styles.btnGroup}>
        <button onClick={save} type="button">OK</button>
        <button onClick={cancel} type="button">Cancel</button>
      </div>
    </div>
  );
};

MessageEditor.propTypes = {
  message: PropTypes.instanceOf(PropTypes.object),
};

MessageEditor.defaultProps = {
  message: {},
};

export default MessageEditor;

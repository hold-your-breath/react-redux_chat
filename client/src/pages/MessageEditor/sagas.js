import {
  all, call, put, takeEvery,
} from 'redux-saga/effects';
import { UPDATE_MESSAGE, UPDATE_MESSAGE_SUCCESS, UPDATE_MESSAGE_FAILED } from './actionTypes';
import { updateMessage as update } from '../../services/domainRequest/messageRequest';

export function* updateMessage(action) {
  const updatedMessage = { ...action.payload.data };

  try {
    yield call(update(updatedMessage));
    yield put({ type: UPDATE_MESSAGE_SUCCESS });
  } catch (err) {
    yield put({ type: UPDATE_MESSAGE_FAILED, err });
  }
}

function* watchUpdateMessage() {
  yield takeEvery(UPDATE_MESSAGE, updateMessage);
}

export default function* messageEditorSagas() {
  yield all([
    watchUpdateMessage(),
  ]);
}

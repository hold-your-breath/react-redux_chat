import {
  all, call, put, takeEvery,
} from 'redux-saga/effects';
import { UPDATE_USER, UPDATE_USER_FAILED, UPDATE_USER_SUCCESS } from './actionTypes';
import { updateUser as update } from '../../services/domainRequest/userRequest';

export function* updateUser(action) {
  const updatedUser = { ...action.payload.data };
  try {
    yield call(update(updatedUser));
    yield put({ type: UPDATE_USER_SUCCESS });
  } catch (err) {
    yield put({ type: UPDATE_USER_FAILED, payload: { error: err } });
  }
}

function* watchUpdateUser() {
  yield takeEvery(UPDATE_USER, updateUser);
}

export default function* userEditorSagas() {
  yield all([
    watchUpdateUser(),
  ]);
}

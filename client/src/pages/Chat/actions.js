import {
  ADD_MESSAGE, DELETE_MESSAGE, EDIT_MESSAGE, LOAD_MESSAGES, FETCH_MESSAGES_SUCCESS,
} from './actionTypes';

export const loadMessagesSuccess = (messages) => ({
  type: FETCH_MESSAGES_SUCCESS,
  payload: {
    messages,
  },
});

export const loadData = () => ({
  type: LOAD_MESSAGES,
});

export const addMessage = (message) => ({
  type: ADD_MESSAGE,
  payload: {
    message,
  },
});

export const editMessage = (id, text) => ({
  type: EDIT_MESSAGE,
  payload: {
    id,
    text,
  },
});

export const deleteMessage = (id) => ({
  type: DELETE_MESSAGE,
  payload: {
    id,
  },
});

import {
  ADD_MESSAGE, DELETE_MESSAGE, EDIT_MESSAGE, FETCH_MESSAGES_SUCCESS, LOAD_MESSAGES,
} from './actionTypes';

export default (state = [], action) => {
  switch (action.type) {
    case LOAD_MESSAGES: {
      return { ...state, loading: true };
    }

    case FETCH_MESSAGES_SUCCESS: {
      return { ...state, messages: action.payload.messages, loading: false };
    }

    case ADD_MESSAGE: {
      const { message } = action.payload;
      return { ...state, messages: [...state.messages, message] };
    }

    case EDIT_MESSAGE: {
      const { id, text } = action.payload;
      const updatedMessage = state.messages.map((message) => {
        if (message.id === id) {
          message.text = text;
          return message;
        }
        return message;
      });

      return { ...state, messages: updatedMessage };
    }

    case DELETE_MESSAGE: {
      const { id } = action.payload;
      const filteredMessages = state.messages.filter((message) => message.id !== id);
      return { ...state, messages: filteredMessages };
    }

    default: return state;
  }
};

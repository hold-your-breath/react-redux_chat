import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MessageInput from '../../containers/MessageInput';
import MessageList from '../../containers/MessageList';
import Header from '../../containers/Header';
import styles from './chat.module.scss';
import {
  addMessage, deleteMessage, editMessage, loadData,
} from './actions';
import Loading from '../../components/Loading';

const Chat = ({
  messages, addMessage: onAdd, deleteMessage: onDelete, editMessage: onEdit, loadData: onLoad,
}) => {
  const [loadedMessages, setLoadedMessages] = useState([]);

  useEffect(() => {
    onLoad();
  }, [onLoad]);

  useEffect(() => {
    if (messages.messages) {
      setLoadedMessages(messages.messages);
    }
  }, [messages]);

  const onAddMessage = (message) => {
    onAdd(message);
  };

  const onDeleteMessage = (id) => {
    // eslint-disable-next-line no-alert
    const isSure = window.confirm('Are you sure?');
    if (isSure) {
      onDelete(id);
    }
  };

  const onEditMessage = (id, text) => {
    onEdit(id, text);
  };

  if (!loadedMessages.length || messages.loading) {
    return (
      <Loading />
    );
  }
  return (
    <div className={styles.chatContainer}>
      <Header messages={loadedMessages} />
      <MessageList messages={loadedMessages} onDelete={onDeleteMessage} onEdit={onEditMessage} />
      <MessageInput onAddMessage={onAddMessage} />
    </div>
  );
};

Chat.propTypes = {
  messages: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.object),
    PropTypes.object,
  ]),
  addMessage: PropTypes.func.isRequired,
  editMessage: PropTypes.func.isRequired,
  deleteMessage: PropTypes.func.isRequired,
  loadData: PropTypes.func.isRequired,
};

Chat.defaultProps = {
  messages: [],
};

const mapStateToProps = (state) => ({
  messages: state.messages,
});

const actions = {
  addMessage,
  deleteMessage,
  editMessage,
  loadData,
};

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Chat);

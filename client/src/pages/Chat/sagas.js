import {
  all, call, put, takeEvery,
} from 'redux-saga/effects';
import {
  LOAD_MESSAGES, FETCH_MESSAGES_SUCCESS, FETCH_MESSAGES_FAILED, ADD_MESSAGE, DELETE_MESSAGE,
} from './actionTypes';
import { getMessages, createMessage, deleteMessage } from '../../services/domainRequest/messageRequest';

export function* fetchMessages() {
  try {
    const messages = yield call(getMessages);
    yield put({ type: FETCH_MESSAGES_SUCCESS, payload: { messages } });
  } catch (err) {
    yield put({ type: FETCH_MESSAGES_FAILED, payload: { error: err } });
  }
}

function* watchFetchMessages() {
  yield takeEvery(LOAD_MESSAGES, fetchMessages);
}

export function* addMessage(action) {
  const { message } = action.payload;
  try {
    yield call(createMessage(message));
    yield put({ type: LOAD_MESSAGES });
  } catch (err) {
    yield put({ type: FETCH_MESSAGES_FAILED, err });
  }
}

function* watchAddMessage() {
  yield takeEvery(ADD_MESSAGE, addMessage);
}

export function* removeMessage(action) {
  const { id } = action.payload;
  try {
    yield call(deleteMessage(id));
    yield put({ type: LOAD_MESSAGES });
  } catch (err) {
    yield put({ type: FETCH_MESSAGES_FAILED, err });
  }
}

function* watchDeleteMessage() {
  yield takeEvery(DELETE_MESSAGE, removeMessage);
}

export default function* chatSagas() {
  yield all([
    watchFetchMessages(),
    watchAddMessage(),
    watchDeleteMessage(),
  ]);
}

import {
  all, call, put, takeEvery,
} from 'redux-saga/effects';
import {
  ADD_USER, DELETE_USER, LOAD_USERS, LOAD_USERS_FAILED, LOAD_USERS_SUCCESS,
} from './actionTypes';
import { createUser, deleteUser, getUsers } from '../../services/domainRequest/userRequest';
import { removeMessage } from '../Chat/sagas';

export function* loadUsers() {
  try {
    const users = yield call(getUsers);
    yield put({ type: LOAD_USERS_SUCCESS, payload: { users } });
  } catch (err) {
    yield put({ type: LOAD_USERS_FAILED, payload: { error: err } });
  }
}

function* watchLoadUsers() {
  yield takeEvery(LOAD_USERS, loadUsers);
}

export function* addUser(action) {
  const { user } = action.payload;
  try {
    yield call(createUser(user));
    yield put({ type: LOAD_USERS });
  } catch (err) {
    yield put({ type: LOAD_USERS_FAILED, err });
  }
}

function* watchAddUser() {
  yield takeEvery(ADD_USER, addUser);
}

export function* removeUser(action) {
  const { id } = action.payload;
  try {
    yield call(deleteUser(id));
    yield put({ type: LOAD_USERS });
  } catch (err) {
    yield put({ type: LOAD_USERS_FAILED, err });
  }
}

function* watchDeleteMessages() {
  yield takeEvery(DELETE_USER, removeMessage);
}

export default function* userListSagas() {
  yield all([
    watchLoadUsers(),
    watchAddUser(),
    watchDeleteMessages(),
  ]);
}

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './userList.module.scss';
import {
  addUser, deleteUser, editUser, loadUsers,
} from './actions';
import Loading from '../../components/Loading';
import User from '../../components/User';

const UserList = ({
  users,
  addUser: onAdd,
  deleteUser: onDelete,
  editUser: onEdit,
  loadUsers: onLoad,
}) => {
  const [loadedUsers, setLoadedUsers] = useState([]);

  useEffect(() => {
    onLoad();
  }, [onLoad]);

  useEffect(() => {
    if (users.users) {
      setLoadedUsers(users.users);
    }
  }, [users]);

  // eslint-disable-next-line no-unused-vars
  const onAddUser = (user) => {
    onAdd(user);
  };

  const onDeleteUser = (user) => {
    onDelete(user);
  };

  const onEditUser = (user) => {
    onEdit(user);
  };

  if (!loadedUsers.length || users.loading) {
    return <Loading />;
  }
  return (
    <div className={styles.container}>
      {loadedUsers.map((user) => (
        <User
          user={user}
          key={user.id}
          deleteUser={onDeleteUser}
          editUser={onEditUser}
        />
      ))}
    </div>
  );
};

UserList.propTypes = {
  users: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.object),
    PropTypes.object,
  ]),
  addUser: PropTypes.func.isRequired,
  editUser: PropTypes.func.isRequired,
  deleteUser: PropTypes.func.isRequired,
  loadUsers: PropTypes.func.isRequired,
};

UserList.defaultProps = {
  users: [],
};

const mapStateToProps = (state) => ({
  users: state.users,
});

const actions = {
  addUser,
  deleteUser,
  editUser,
  loadUsers,
};

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserList);

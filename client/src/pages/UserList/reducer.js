import {
  ADD_USER, DELETE_USER, EDIT_USER, LOAD_USERS, LOAD_USERS_SUCCESS,
} from './actionTypes';

export default (state = [], action) => {
  switch (action.type) {
    case LOAD_USERS: {
      return { ...state, loading: true };
    }

    case LOAD_USERS_SUCCESS: {
      return { ...state, users: action.payload.users, loading: false };
    }

    case ADD_USER: {
      const { user } = action.payload;
      return { ...state, users: [...state.users, user] };
    }

    case EDIT_USER: {
      const { user } = action.payload;
      const updatedUser = state.users.map((item) => {
        if (item.id === user.id) {
          // eslint-disable-next-line no-param-reassign
          item = user;
          return item;
        }
        return item;
      });

      return { ...state, users: updatedUser };
    }

    case DELETE_USER: {
      const { id } = action.payload;
      const filteredUsers = state.users.filter((user) => user.id !== id);
      return { ...state, users: filteredUsers };
    }

    default: return state;
  }
};

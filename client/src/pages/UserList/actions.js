import {
  ADD_USER, LOAD_USERS, LOAD_USERS_SUCCESS, EDIT_USER, DELETE_USER,
} from './actionTypes';

export const loadUsersSuccess = (users) => ({
  type: LOAD_USERS_SUCCESS,
  payload: {
    users,
  },
});

export const loadUsers = () => ({
  type: LOAD_USERS,
});

export const addUser = (user) => ({
  type: ADD_USER,
  payload: {
    user,
  },
});

export const editUser = (user) => ({
  type: EDIT_USER,
  payload: {
    user,
  },
});

export const deleteUser = (id) => ({
  type: DELETE_USER,
  payload: {
    id,
  },
});

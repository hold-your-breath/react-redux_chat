import { all } from 'redux-saga/effects';
import chatSagas from '../pages/Chat/sagas';
import loginSagas from '../pages/Login/sagas';
import messageEditorSagas from '../pages/MessageEditor/sagas';
import userEditorSagas from '../pages/UserEditor/sagas';
import userListSagas from '../pages/UserList/sagas';

export default function* rootSaga() {
  yield all([
    chatSagas(),
    loginSagas(),
    messageEditorSagas(),
    userEditorSagas(),
    userListSagas(),
  ]);
}

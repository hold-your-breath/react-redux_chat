const { HTTP_REQUEST_SUCCESS } = require('../helpers/constants');

const responseMiddleware = (req, res, next) => {
  if(res.data) {
    return res.status(HTTP_REQUEST_SUCCESS).json(res.data);
  }
  if(res.err) {
    const { statusCode } = res.err;
    return res.status(statusCode).json({ error: true, message: res.err.message });
  }
}

exports.responseMiddleware = responseMiddleware;
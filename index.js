const express = require('express');
const cors = require('cors');
require('./config/db');
const { PORT } = require('./helpers/constants');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const routes = require('./routes/index');
routes(app);

app.use('/', express.static('./client/build'));

app.listen(PORT, () => {console.log(`listening on http://localhost:${PORT}`)});

exports.app = app;
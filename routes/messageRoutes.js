const { Router } = require('express');
const MessageService = require('../services/messageService');
const { HTTP_NOT_FOUND } = require('../helpers/constants');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router
  .get('/', (req, res, next) => {
    try {
      const data = MessageService.getAll();
      res.data = data;
    } catch (err) {
      err.statusCode = HTTP_NOT_FOUND;
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .get('/:id', (req, res, next) => {
    try {
      const id = req.params.id;
      const data = MessageService.getOne(id);
      res.data = data;
    } catch (err) {
      err.statusCode = HTTP_NOT_FOUND;
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .post('/', (req, res, next) => {
    try {
      const messageData = req.body;
      console.log(messageData);
      const data = MessageService.create(messageData);
      res.data = data;
    } catch (err) {
      err.statusCode = HTTP_NOT_FOUND;
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .put('/:id', (req, res, next) => {
    try {
      const messageId = req.params.id;
      const messageData = req.body;
      const data = MessageService.update(messageId, messageData);
      res.data = data;
    } catch (err) {
      err.statusCode = HTTP_NOT_FOUND;
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .delete('/:id', (req, res, next) => {
    try {
      const id = req.params.id;
      const data = MessageService.delete(id);
      res.data = data;
    } catch (err) {
      err.statusCode = HTTP_NOT_FOUND;
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware);

  module.exports = router;
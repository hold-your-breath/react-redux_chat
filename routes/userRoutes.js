const { Router } = require('express');
const UserService = require('../services/userService');
const { HTTP_NOT_FOUND } = require('../helpers/constants');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router
  .get('/', (req, res, next) => {
    try {
      const data = UserService.getAll();
      res.data = data;
    } catch (err) {
      err.statusCode = HTTP_NOT_FOUND;
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .get('/:id', (req, res, next) => {
    try {
      const id = req.params.id;
      const data = UserService.getOne(id);
      res.data = data;
    } catch (err) {
      err.statusCode = HTTP_NOT_FOUND;
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .post('/', (req, res, next) => {
    try {
      const userData = req.body;
      const data = UserService.create(userData);
      res.data = data;
    } catch (err) {
      err.statusCode = HTTP_NOT_FOUND;
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .put('/:id', (req, res, next) => {
    try {
      const userId = req.params.id;
      const userData = req.body;
      const data = UserService.update(userId, userData);
      res.data = data;
    } catch (err) {
      err.statusCode = HTTP_NOT_FOUND;
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .delete('/:id', (req, res, next) => {
    try {
      const id = req.params.id;
      const data = UserService.delete(id);
      res.data = data;
    } catch (err) {
      err.statusCode = HTTP_NOT_FOUND;
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware);

module.exports = router;